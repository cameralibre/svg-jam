export default [
  [
    { author: 'sam', text: 'can this be a bit bigger?', timestamp: new Date(2019, 5, 20, 10).getTime() },
    { author: 'mix', text: 'yeah I agree, but max 10% image width', timestamp: new Date(2019, 5, 20, 11).getTime() },
    { author: 'sam', text: 'are you sure, this needs to be *punchy*', timestamp: new Date(2019, 5, 20, 12).getTime() }
  ], [
    { author: 'sam', text: 'I like this, more of these', timestamp: new Date(2019, 5, 21, 13).getTime() },
    { author: 'mix', text: 'I could not disagree more', timestamp: new Date(2019, 5, 21, 15).getTime() }
  ]
]
