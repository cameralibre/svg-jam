export default function isEmpty (thing) {
  if (Array.isArray(thing)) return thing.length === 0

  // if (thing.hasOwnProperty && thing.hasOwnProperty('length')) {
  //   return thing.length === 0
  // }
  // could this replace the isArray code? let the tests decide
}
