const TYPES = [ 'path', 'circle', 'rect', 'polygon', 'ellipse', 'use', 'text' ]
function getShapes () {
  return TYPES
    .reduce(
      (acc, type) => [
        ...acc,
        ...SVG.select(type).members // eslint-disable-line
      ],
      [] // initial value for acc (accumulator)
    )
}

function loadDrawing (root, innerHTML) {
  root.innerHTML = innerHTML
  let el = root.getElementsByTagName('svg')[0]

  if (!el) throw new Error(`loadDrawing failed to load svg, did you upload something odd?`)

  const drawing = SVG.adopt(el) // eslint-disable-line
  drawing.attr('width', '100%').attr('height', '100%')

  return drawing
}

function highlight (shape) {
  shape.selectize({ points: [], rotationPoint: false })
}

function unHighlight (shape) {
  shape.selectize(false)
}

export {
  getShapes,
  loadDrawing,
  highlight,
  unHighlight
}
