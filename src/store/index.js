import Vue from 'vue'
import Vuex from 'vuex'

import svg from './modules/svg'
import comments from './modules/comments'
import createLogger from './plugins/logger'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    svg,
    comments
  },
  strict: debug,
  plugins: debug ? [createLogger()] : []
})
