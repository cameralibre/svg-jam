const state = {
  visible: false,
  author: localStorage.svgJamAuthor || '', // eslint-disable-line
  comments: []
}

const getters = {
}

const mutations = {
  reset (state) {
    state.comments = []
  },
  toggle (state) {
    state.visible = !state.visible
  },
  loadComments (state, shape) {
    if (typeof shape.data !== 'function') throw new Error('loadComments expects shape.data to be a function')

    const comments = shape.data('comments')
    // TODO check it's an Array, and that each element is valid { author, text, timestamp }
    state.comments = comments || []
  },
  setComments (state, comments) {
    state.comments = [...comments]
  },
  setAuthor (state, name) {
    if (typeof name !== 'string') throw new Error(`setAuthor expects a string, got ${typeof name}`)
    if (!name.length) return

    state.author = name
    localStorage.svgJamAuthor = name // eslint-disable-line
    // TODO make this an action (as has side-effects)
  }
}

const actions = {
  publishComment ({ commit, dispatch, state }, text) {
    // I register this as an action, and not a mutation because it has side-effects
    // outside of just changing the state (e.g. it modifies to SVG)

    if (typeof text !== 'string') throw new Error(`publishComment expects a string, got ${typeof text}`)
    text = text.trim()
    if (!text.length) return

    const comments = [
      ...state.comments,
      {
        author: state.author,
        text,
        timestamp: Date.now()
      }
    ]

    commit('setComments', comments)
    dispatch('svg/setComments', comments, { root: true })
  }
}

export default {
  state,
  getters,
  mutations,
  actions,
  namespaced: true
}
