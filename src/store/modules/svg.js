const state = {
  fileName: '',
  selectedCount: 0
}

const getters = {
  canComment: (state) => {
    return state.selectedCount === 1
  }
}

const mutations = {
  reset (state) {
    // state.fileName = ''
    state.selectedCount = 0
  },
  setState (state, newState) {
    Object.keys(state).forEach(key => {
      if (newState[key]) state[key] = newState[key]
    })
  }
}

const actions = {
  upload ({ state, dispatch, commit }, payload) {
    commit('reset')

    dispatch('setFilename', payload.fileName)
    dispatch('setInnerHTML', payload.svgText)
  },
  setFilename ({ commit }, payload) {
    commit('setState', { fileName: payload })
  },
  setInnerHTML (context, h) {
    // see the componenets/SvgContainer for the handling of this
  },
  setComments (context, comments) {
    // see the componenets/SvgContainer for the handling of this
  },
  download (context, comments) {
    // see the componenets/SvgContainer for the handling of this
  }
}

export default {
  state,
  getters,
  mutations,
  actions,
  namespaced: true
}
