import isEmpty from '@/util/is-empty.js'
// @ gets replaced with the path to src!!

// test and expect are functions that jest just magically supplies?
// hmmm

describe('isEmpty', () => {
  test('Arrays', () => {
    expect(isEmpty([])).toBe(true)
    expect(isEmpty([2, 4])).toBe(false)
  })

  test('Strings', () => {
    // write some tests
  })

  test('Objects', () => {
    // write some tests
  })

  test('Numbers', () => {
    // write some tests
    // - integer
    // - negative numbers
    // - floats (e.g. 3.14159)
  })

  // what about undefined / null?
})

// Further reading
// - Check out other sorts of matchers: https://jestjs.io/docs/en/using-matchers
