import { shallowMount } from '@vue/test-utils'
// docs : https://vue-test-utils.vuejs.org/
// we don't really need to get into this at the moment, but can check it out in future.

import Button from '@/components/ToggleCommentsButton'

describe('ToggleCommentsButton.vue', () => {
  it('renders props.msg when passed', () => {
    const wrapper = shallowMount(Button)

    expect(wrapper.text()).toMatch('Toggle')

    // const msg = 'new message'
    // const wrapper = shallowMount(HelloWorld, {
    //   propsData: { msg }
    // })
    //
    // expect(wrapper.text()).toMatch(msg)
  })
})
